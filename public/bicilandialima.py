# pip install Flask

from flask import Flask, render_template, request, jsonify
import urllib.request
import json

app = Flask(__name)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # Obtener los datos ingresados por el usuario desde el formulario web
        fecha = request.form['fecha']
        estacion = request.form['estacion']
        registrado = request.form['registrado']

        # Resto del código para enviar la solicitud de predicción
        # ... (mismo código que proporcionaste)

        try:
            response = urllib.request.urlopen(req)
            result = response.read().decode("utf-8")
            response_json = json.loads(result)

            # Extraer el valor de resultado y registrado
            resultado = response_json["Results"]["output1"]["value"]["Values"][0][-1]
            resultado_registrado = response_json["Results"]["output1"]["value"]["Values"][0][-2]
            return f"El resultado de la predicción es: {resultado}"
        except urllib.error.HTTPError as error:
            return f"La solicitud falló con el código de estado: {str(error.code)}"

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)




